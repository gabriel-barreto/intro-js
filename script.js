function digaOla() {
    alert('Olá!');
}

$('document').ready(function() {
    //alert('Olá!');
    $('#button-ola').text('Gabriel');
    $('#button-ola').delay(350).fadeIn();
});

$('#button-ola').click(function() {
    digaOla();
    $('#button-ola').delay(250).fadeOut();
});